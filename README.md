# Git_branches

The idea is to be able to clone every branches of your project on staging server
and use like <branch>.staging.mydomain.com

# Installation
1/ download git.sh  
2/ chmod +x git.sh  
3/ create a folder outside from /var/www (example mkdir ~/test-dashboard)  
4/ cd ~/test-dashboard  
5/ clone your repository (git clone as ssh git clone git@bitbucket.org:fflearning/dashboard.git . )  
6/ move the git.sh into this file (mv ./git.sh ~/test-dashboard/git.sh)  
7/ make git.sh executable (chmod -x ~/test-dashboard/git.sh)  
8/ run ~/test-dashboard/git.sh to TEST  
9/ setup it as ~/test-dashboard/git.sh as cronjob  
10/ then config apache or nginx and test then test  

# Options
    -e | --environement set the environement you want, You will have access into install.sh with $ENVIRONEMENT  
    -d | --directory set git folder  
    -b | --branch specify one branch  
    --force-install for the run of install.sh even if "Already-up-to-date"
    --by-tags when you want to have a setup by tags instead of branches
    --git-directory you can specify your git root folder

# NGINX config example
server {  
    server_name ~^(?<branch>.*)\.staging\.mydomain\.com$;  
    access_log /var/log/nginx/branch-access.log;  
    error_log /var/log/nginx/branch-error.log;  
    index index.php index.html;  
    root /var/www/test-dashboard/$branch;  
}  

instead of $document_root use $realpath_root, example:
fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;


# APACHE config example
<VirtualHost *:80>  
        ServerName staging.mydomain.com  
        ServerAlias *.mydomain.com  
        VirtualDocumentRoot /var/www/test-dashboard/%1/  
</VirtualHost>  


# install.sh
into the root of your git you can create an install.sh  
to run all the installation you need such like   
composer install ect ....  
you have access to some variable who are set directly from the main script   
$ENVIRONEMENT (string) will provide the current environement for setup such like (prod, staging, dev)  
$myBranch (string) the current branch  
