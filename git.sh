#!/bin/bash
BRANCH="xFALSE"
CREATE_SYMLINK="xFALSE"
export ENVIRONEMENT="staging"
directory="xFALSE"
FORCE_INSTALL="xFALSE"
BY_TAGS="xFALSE"
SCRIPT=`realpath $0`
current_dir=`dirname $SCRIPT`


while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    shift # past value
    ;;
    -e|--environement)
    export ENVIRONEMENT="$2"
    shift # past argument
    shift # past value
    ;;
    -d|--directory)
    export directory="$2"
    shift # past argument
    shift # past value
    ;;
    --force-install)
    FORCE_INSTALL="xTRUE"
    shift # past argument
    ;;
    --by-tags)
    BY_TAGS="xTRUE"
    shift # past argument
    ;;
    --git-directory)
    cd "$2"
    shift # past argument
    shift # past value
    ;;
    --create-symlink)
    CREATE_SYMLINK="$2"
    shift # past argument
    shift # past value
    ;;
    *)
        echo "$key is not a valid option"
        exit
    ;;
esac
done


#if [ ! -f '.git-akktis' ]
#then
#   touch .git-akktis
#else
#   source .git-akktis
#fi

if [ $directory == "xFALSE" ]
then
        echo "-d directory is mandatory"
        exit
fi

if [ ! -d $directory ]
then
        echo "the folder ($directory) is not exist"
        exit
fi

#echo "export directory=\"$directory\"
#export BRANCH=\"$BRANCH\"
#export ENVIRONEMENT=\"$ENVIRONEMENT\"
#export FORCE_INSTALL=\"$FORCE_INSTALL\"
#export BY_TAGS=\"$BY_TAGS\"" > .git-akktis

echo "Run on environement $ENVIRONEMENT"
echo "on directory $directory"

echo "git fetch"
# Fetch the latest branches
git fetch --all -a -p

# Get the repository path
repoPath=`git config --get remote.origin.url`

if [ $BY_TAGS == "xTRUE" ]
then
    gitBranches=`git tag -l`
else
    echo "git for-each-ref"
    gitBranches=`git for-each-ref --format='%(refname)' refs/remotes/`
fi

echo $directory

dirs=`ls $directory`

for d in $dirs
do
     found="xFALSE"
     for branches in $gitBranches
     do
        
        export myBranch=${branches/refs\/remotes\/origin\//}

        export myBranchWithoutCap=$(echo "$myBranch" | tr '[:upper:]' '[:lower:]' | tr '/' '-' | tr '_' '-')
        
       
        if [ "$myBranchWithoutCap" == "$d" ]
        then
           
           found="xTRUE"
        fi
    done

    if [ "$found" == "xFALSE" ]
    then
         
         rm -rf "$directory/$d"
    fi
done

# Loop through each branch
for branches in $gitBranches
do
    if [ $BY_TAGS == "xTRUE" ]
        then
        export myBranch=$branches

        export myBranchWithoutCap=$(echo "$myBranch" | tr '[:upper:]' '[:lower:]' | tr '/' '-' | tr '_' '-')
    else
        # Remove redundant string from branch name
            export myBranch=${branches/refs\/remotes\/origin\//}

            export myBranchWithoutCap=$(echo "$myBranch" | tr '[:upper:]' '[:lower:]' | tr '/' '-' | tr '_' '-')
    fi


        # Check if we are using 'HEAD' branch
        if [ "$myBranch" == "HEAD" ]
        then
                continue
        fi


        #checking if we specify a branch
        if [ $BRANCH != "xFALSE" ]
        then
                if [  $BRANCH != "$myBranch" ]
                then
                        continue
                fi
        fi

        echo "Fetching $myBranch"
        # Check if the branch has already been checked out
        
        if [ -e "$directory/$myBranchWithoutCap" ]
        then
                # Switch to the branch directory
                cd "$directory/$myBranchWithoutCap"
                
                git config --global --add safe.directory "$directory/$myBranchWithoutCap"

                #checking if the branch is still exist otherwise we remove it
                git ls-remote --heads --tags origin | grep -E "refs/(heads|tags)/${myBranch}$" >/dev/null

                if [[ "$?" -eq "0" ]]; then
                        # Pull latest code
                        status=`git pull origin $myBranch`

                        # Check if there are any changes
                        if [ "$status" == "Already up-to-date." ] || [ "$status" == "Already up to date." ]
                        then
                                if [ $FORCE_INSTALL == "xFALSE" ]
                                then
                                        continue
                                fi
                        fi
                else
                        echo "Removing $myBranch"
                        rm -rf "$directory/$myBranchWithoutCap"
                        echo "directory: $directory/$myBranchWithoutCap"
                        continue
                fi
        else
            # Switch to branches directory
            cd $directory

            # Clone repository to branch named directory
            git clone $repoPath "$myBranchWithoutCap"

            # Switch to specific branch directory
            cd "$directory/$myBranchWithoutCap"

            # Switch to the branch
            git checkout $myBranch

        fi
       
    #checking if install.sh is existing
	echo "Check if install.sh exists!" 
    if [ -e "$directory/$myBranchWithoutCap"/install.sh ]
        then
        echo "Runing install.sh"
                chmod +x "$directory/$myBranchWithoutCap"/install.sh
                cd "$directory/$myBranchWithoutCap"/
                ./install.sh
		echo "install.sh runned succesful!"
    else
	echo "install.sh not existing"

    fi

done

if [ $CREATE_SYMLINK != "xFALSE" ]
then
    if [ -e $CREATE_SYMLINK ]
    then
        rm $CREATE_SYMLINK
    fi
    if [ ! -d "$directory/$BRANCH" ]
    then
        if [ ! -d "$directory/master" ]
        then
            ln -s "$directory/master" $CREATE_SYMLINK
        else
             echo "ERROR : NOT ABLE TO CREATE SYMLINK ";
        fi
    else
      ln -s "$directory/$BRANCH" $CREATE_SYMLINK
    fi
    if [ ! -e $CREATE_SYMLINK ]
    then
        ln -s "$directory/master" $CREATE_SYMLINK
    fi
fi

if [ -f "$current_dir/custom.sh" ]; then
        cd "$current_dir"
        ./custom.sh
else
	echo "$current_dir/custom.sh not existing"
fi
